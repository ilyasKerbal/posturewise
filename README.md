# PostureWise
## App screenshots

| Intro | Main | Exercises | 30 Days Program | Stats |
|--|--|--|--|--|
| ![Screenshot](Screenshots/promo-screen.png) | ![Screenshot](Screenshots/promo-screen01.png) | ![Screenshot](Screenshots/promo-screen02.png) | ![Screenshot](Screenshots/promo-screen03.png) | ![Screenshot](Screenshots/promo-screen04.png) |

| Rating | Exercises | Promo design | Exercise | Rating feedback |
|--|--|--|--|--|
| ![Screenshot](Screenshots/promo-screen05.png) | ![Screenshot](Screenshots/promo-screen06.png) | ![Screenshot](Screenshots/promo-screen07.jpg) | ![Screenshot](Screenshots/promo-screen08.jpg) | ![Screenshot](Screenshots/promo-screen09.jpg)